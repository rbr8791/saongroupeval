## Introduction

DotNet Saongroup Evaluation test

# Requirements

- [Microsoft dotnet core framework](https://dotnet.microsoft.com/download) 2.2.0


## Run the project

- Download de code from: https://gitlab.com/rbr8791/saongroupeval.git
- Change to the main solution directory
- run the following script
	- Windows:
		- ``` runapp.cmd ```
	- Linux:
		- ``` runapp.sh ```
- Open the following URL:
	- http://localhost:5000/swagger/index.html

## Test the API

- Authenticate:
	- use the API route:
		- /api/v1/User/authenticate
		- Body data:
			- ``` 
				{
					 "username": "admin",
					 "password": "admin"
				}
			  ```
		- Sample response:
			- ```
				{
				  "id": 1,
				  "username": "admin",
				  "firstName": "admin",
				  "lastName": "admin",
				  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjEiLCJuYmYiOjE1OTg4NTc3NzAsImV4cCI6MTU5OTQ2MjU3MCwiaWF0IjoxNTk4ODU3NzcwfQ.syid6yupY9Rf1BL3--0NXaJozAk5Ab61dUlridPcHBE"
				}
		- Copy the Bearer authentication token response in the top right Authorize section and you can start to test the API routes

- Job Dashboard routes
	- GET /api/v1/Job/Get/{id} 
		- Get a Job entry by id
	- GET /api/v1/Job
		- Get all the Job dashboard entries
	- DELETE /api/v1/Job/{id}
		- Delete a Job entry dashboard by id
	- POST /api/v1/Job/Create
		- Create a new Job entry dashboard
		- Body data:
			- ```
				{
					"id": 0,
					"jobTitle": "Job Title 100",
					"description": "Job Title description 100",
					"expiresAt": "2020-08-31 06:05:48"
				}
			  ```
	- POST /api/v1/Job/Update
		- Update a Job entry
		- Body data:
			- ```
				{
					"id": 1,
					"jobTitle": "Job Title 001 - Updated",
					"description": "Job Title description - value updated",
					"expiresAt": "2020-05-30 09:05:48"
				}

## Author
-	Name: R@ul Berrios Rivera
-	Email: raul.berrios@outlook.com
