﻿using System;



    namespace saongroup.jobdashboard.api.entities
    {
        public class BaseAuditClass
        {
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

            public DateTime ModifiedAt { get; set; } = DateTime.UtcNow;

            public string createdBy { get; set; } = "";
        }
    }
    

