﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace saongroup.jobdashboard.api.entities
{
    public class Role: BaseAuditClass
    {
        public int Id { get; set; }
        [Required]
        public string RoleName { get; set; }
        [Required]
        public bool Status { get; set; } = true;
    }
}
