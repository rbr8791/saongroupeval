﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using saongroup.jobdashboard.api.entities;


namespace saongroup.jobdashboard.api.persistence
{
    public class SaonGroupJobDashboardDbContext: DbContext, ISaonGroupJobDashboardDbContext
    {
        public SaonGroupJobDashboardDbContext(DbContextOptions<SaonGroupJobDashboardDbContext> options) : base(options) { }
        public SaonGroupJobDashboardDbContext() { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddPrimaryKeys();
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            ChangeTracker.ApplyAuditInformation();

            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }


        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Job> Jobs { get; set; }
    }
}
