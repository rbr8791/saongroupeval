﻿#!/bin/bash

echo "You need to have dotnet environment installed in your O.S"
echo "Install dotnet ef version requirement"
dotnet tool install --global dotnet-ef --version 3.0.0

echo "Creating initial migration"
dotnet ef migrations add InitialMigration

echo "Running database updates"
dotnet ef database update

echo "Copying sqlite database file to running directory, this will overwrite any existing db file"
cp -rf saongroupJobDashboardAPI.db ../saongroup-jobdashboard-api/saongroupJobDashboardAPI.db

echo "We will start the database seeding process..."

echo "cd ../saongroup-jobdashboard-api"
cd ../saongroup-jobdashboard-api
echo "Building project..."
dotnet build

echo "Running project for fist time and seeding the database"

dotnet run



