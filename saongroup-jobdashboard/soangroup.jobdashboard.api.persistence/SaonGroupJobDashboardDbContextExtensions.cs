﻿using System.IO;
using System.Linq;
using saongroup.jobdashboard.api.persistence.Helpers;
using saongroup.jobdashboard.api.persistence;
using saongroup.jobdashboard.api.entities;

namespace saongroup.jobdashboard.api.persistence
{
    public static class SaonGroupJobDashboardDbContextExtensions
    {
        public static int EnsureSeedData(this SaonGroupJobDashboardDbContext context)
        {
            var rolesCount = default(int);
          
            var dbSeeder = new DatabaseSeeder(context);

            
            if (!context.Roles.Any())
            {
                var pathToSeedData = Path.Combine(Directory.GetCurrentDirectory(), "SeedData", "RoleSeedData.json");
                rolesCount = dbSeeder.SeedRolesFromJson(pathToSeedData).Result;
                if (rolesCount > 0)
                {


                    int adminId = context
                      .Roles
                      .Where(r => r.RoleName == "Admin")
                      .Select(r => r.Id)
                      .SingleOrDefault();

                    Role role = context.Roles.Find(adminId);

                    if (adminId > 0 && role != null)
                    {
                        byte[] passwordHash, passwordSalt;
                        var password = "admin";
                        Tools.CreatePasswordHash(password, out passwordHash, out passwordSalt);

                        // Add user Admin
                        var user = new User();
                        user.FirstName = "admin";
                        user.LastName = "admin";
                        user.Username = "admin";
                        user.Password = "";
                        user.PasswordHash = passwordHash;
                        user.PasswordSalt = passwordSalt;
                        user.Role = role;
                        user.Enabled = true;

                        context.Users.Add(user);
                        context.SaveChanges();
                    }

                    int operatorId = context
                      .Roles
                      .Where(r => r.RoleName == "Operator")
                      .Select(r => r.Id)
                      .SingleOrDefault();

                    Role roleOperator = context.Roles.Find(operatorId);

                    
                    if (operatorId > 0 && roleOperator != null)
                    {
                        byte[] passwordHash, passwordSalt;
                        var password = "operator";
                        Tools.CreatePasswordHash(password, out passwordHash, out passwordSalt);

                        // Add user Admin
                        var userOperator = new User();
                        userOperator.FirstName = "operator";
                        userOperator.LastName = "operator";
                        userOperator.Username = "operator";
                        userOperator.Password = "";
                        userOperator.PasswordHash = passwordHash;
                        userOperator.PasswordSalt = passwordSalt;
                        userOperator.Role = roleOperator;
                        userOperator.Enabled = true;

                        context.Users.Add(userOperator);
                        context.SaveChanges();
                    }


                    if (operatorId > 0 && roleOperator != null)
                    {
                        byte[] passwordHash, passwordSalt;
                        var password = "operator";
                        Tools.CreatePasswordHash(password, out passwordHash, out passwordSalt);

                        // Add user Admin
                        var userOperator = new User();
                        userOperator.FirstName = "operator";
                        userOperator.LastName = "operator";
                        userOperator.Username = "operator";
                        userOperator.Password = "";
                        userOperator.PasswordHash = passwordHash;
                        userOperator.PasswordSalt = passwordSalt;
                        userOperator.Role = roleOperator;
                        userOperator.Enabled = true;

                        context.Users.Add(userOperator);
                        context.SaveChanges();
                    }

                    
                }
            }



            return rolesCount;
        }
    }
}
