﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using saongroup.jobdashboard.api.common;

namespace saongroup.jobdashboard.api.persistence
{
    public class SaonGroupJobDashboardDbContextFactory : IDesignTimeDbContextFactory<SaonGroupJobDashboardDbContext>
    {
        private static string DbConnectionString => new DatabaseConfiguration().GetDatabaseConnectionString();

        public SaonGroupJobDashboardDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SaonGroupJobDashboardDbContext>();

            optionsBuilder.UseSqlite(DbConnectionString);

            return new SaonGroupJobDashboardDbContext(optionsBuilder.Options);
        }
    }
}
