﻿using System.Text;
using System.Collections.Generic;
using System.Linq;
using saongroup.jobdashboard.api.entities;
using saongroup.jobdashboard.api.persistence;
using Microsoft.EntityFrameworkCore;
using saongroup.jobdashboard.api.dal.Interfaces;
using System;

namespace saongroup.jobdashboard.api.dal
{
    public class JobService : IJobService
    {
       
        private SaonGroupJobDashboardDbContext _context;

        public JobService(SaonGroupJobDashboardDbContext context)
        {
            _context = context;
        } // End DataContext

        public Job FindByOrdinal(int id)
        {
            return BaseQuery()
                .FirstOrDefault(job => job.Id == id);
        }

        private IEnumerable<Job> BaseQuery()
        {

            return _context.Jobs;

        }
        public IEnumerable<Job> GetAll()
        {
            return BaseQuery();
            
        } // End IEnumerable

        public Job GetById(int id)
        {
            return BaseQuery()
                .FirstOrDefault(role => role.Id == id);
        } // End GetById

        public Job Create(Job job, string jobTitle, string description, string ex_at)
        {
            // Validation
            if (string.IsNullOrWhiteSpace(jobTitle))
                throw new System.ApplicationException("The Job title can not be null or empty");

            job.JobTitle = jobTitle;
            job.Description = description;
            
            job.ExpiresAt = DateTime.Parse(ex_at);
          

            _context.Jobs.Add(job);
            _context.SaveChanges();

            return job;

        } // End Create


        public Job Update(Job job, string jobTitle, string description, string expiresAt)
        {
            var j = _context.Jobs.Find(job.Id);
            if (j == null)
                throw new System.ApplicationException("Job not found");

            


            j.JobTitle = jobTitle;
            j.Description = description;
            j.ExpiresAt = DateTime.Parse(expiresAt);


            _context.Jobs.Update(j);
            _context.SaveChanges();
            return j;
        } // End Update


        public void Delete(int id)
        {
            var job = _context.Jobs.Find(id);
            if (job != null)
            {
                _context.Jobs.Remove(job);
                _context.SaveChanges();
            }
        } // End Delete
    }
}
