﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using saongroup.jobdashboard.api.entities;
using saongroup.jobdashboard.api.persistence;
using Microsoft.EntityFrameworkCore;
using saongroup.jobdashboard.api.dal.Interfaces;

namespace saongroup.jobdashboard.api.dal
{
    public class DatabaseService : IDatabaseService
    {
        private readonly SaonGroupJobDashboardDbContext _context;
        public DatabaseService(SaonGroupJobDashboardDbContext context)
        {
            _context = context;
        }
        public bool ClearDatabase()
        {
            var cleared = _context.Database.EnsureDeleted();
            var created = _context.Database.EnsureCreated();
            var entitiesadded = _context.SaveChanges();

            return (cleared && created && entitiesadded == 0);
        }

        public int SeedDatabase()
        {
            return _context.EnsureSeedData();
        }



        public async Task<int> SaveAnyChanges()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
