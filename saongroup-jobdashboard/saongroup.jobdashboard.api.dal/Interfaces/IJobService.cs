﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Generic;
using saongroup.jobdashboard.api.entities;

namespace saongroup.jobdashboard.api.dal.Interfaces
{
    public interface IJobService
    {
        IEnumerable<Job> GetAll();
        Job GetById(int id);
        Job Create(Job job, string jobTitle, string description, string expiresAt);
        Job Update(Job job, string jobTitle, string description, string expiresAt);
        void Delete(int id);
        Job FindByOrdinal(int id);
    }
}
