﻿using System;
using System.Collections.Generic;
using System.Text;
using saongroup.jobdashboard.api.entities;
using System.Threading.Tasks;

namespace saongroup.jobdashboard.api.dal.Interfaces
{
    public interface IDatabaseService
    {
        bool ClearDatabase();

        int SeedDatabase();
               
        Task<int> SaveAnyChanges();
    } // End Interface
}
