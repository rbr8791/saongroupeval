﻿using System;
using System.Collections.Generic;
using System.Text;
using saongroup.jobdashboard.api.entities;

namespace saongroup.jobdashboard.api.dal.Interfaces
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
        User GetById(int id);
        User Create(User user, string password);
        void Update(User user, string password = null);
        void Delete(int id);
        User FindByOrdinal(int id);
    }
}
