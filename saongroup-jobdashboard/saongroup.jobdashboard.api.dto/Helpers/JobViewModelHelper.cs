﻿using System;
using System.Collections.Generic;
using System.Linq;
using saongroup.jobdashboard.api.dto.ViewModels;
using saongroup.jobdashboard.api.entities;
using saongroup.jobdashboard.api.dto.MapperObjects;

namespace saongroup.jobdashboard.api.dto.Helpers
{
    public static class JobViewModelHelper
    {
        public static JobViewModel ConvertToViewModel(Job dbModel)
        {
            var viewModel = new JobViewModel
            {
                Id = dbModel.Id,
                JobTitle = dbModel.JobTitle,
                Description = dbModel.Description,
                ExpiresAt = dbModel.ExpiresAt,
            };

            return viewModel;
        }

        public static List<JobViewModel> ConvertToViewModels(List<Job> dbModel)
        {
            return dbModel.Select(ConvertToViewModel).ToList();
        }

        public static List<JobBaseViewModel> ConvertToBaseViewModels(List<Job> dbModel)
        {
            return dbModel.Select(ConvertToBaseViewModel).ToList();
        }


        private static JobBaseViewModel ConvertToBaseViewModel(Job dbModel)
        {
            var viewModel = new JobBaseViewModel
            {
                Id = dbModel.Id,
                JobTitle = dbModel.JobTitle,
                Description = dbModel.Description,
                ExpiresAt = dbModel.ExpiresAt,
            };


            return viewModel;
        }
    } // End Class
}
