﻿using System;
using System.Collections.Generic;
using System.Text;
using saongroup.jobdashboard.api.dto.MapperObjects;

namespace saongroup.jobdashboard.api.dto.ViewModels
{
    public class RoleBaseViewModel : BaseViewModel
    {
        public int Id { get; set; }

        public string RoleName { get; set; }

        public bool Status { get; set; }
    }
}
