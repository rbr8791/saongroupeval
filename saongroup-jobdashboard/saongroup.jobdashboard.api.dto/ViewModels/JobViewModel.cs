﻿using System;
using System.Collections.Generic;
using System.Text;

namespace saongroup.jobdashboard.api.dto.ViewModels
{
    public class JobViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string JobTitle { get; set; }
        public string Description { get; set; }
        public DateTime ExpiresAt { get; set; } 
    }
}
