﻿using System;
using System.Collections.Generic;
using System.Text;

namespace saongroup.jobdashboard.api.dto.ViewModels
{
    public class RoleViewModel : BaseViewModel
    {
        public int Id { get; set; }

        public string RoleName { get; set; }

        public bool Status { get; set; }
    }
}
