﻿using System;
using System.Collections.Generic;
using System.Text;
using saongroup.jobdashboard.api.dto.Helpers.Infrastructure;
using saongroup.jobdashboard.api.entities;
using System.Linq.Expressions;
using System.Linq;

namespace saongroup.jobdashboard.api.dto.MapperObjects
{
    public class JobDto
    {
        public int Id { get; set; }
        public string JobTitle { get; set; }
        public string Description { get; set; }
        public DateTime ExpiresAt { get; set; }

        public static JobDto FromModel(Job model)
        {
            return new JobDto()
            {
                Id = model.Id,
                JobTitle = model.JobTitle,
                Description = model.Description,
                ExpiresAt = model.ExpiresAt,
            };
        }

        public Job ToModel()
        {
            return new Job()
            {
                Id = Id,
                JobTitle = JobTitle,
                Description = Description,
                ExpiresAt = ExpiresAt,
            };
        }
    }
}
