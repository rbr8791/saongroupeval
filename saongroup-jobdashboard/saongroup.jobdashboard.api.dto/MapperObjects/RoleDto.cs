﻿using System;
using System.Collections.Generic;
using System.Text;
using saongroup.jobdashboard.api.dto.Helpers.Infrastructure;
using saongroup.jobdashboard.api.entities;
using System.Linq.Expressions;
using System.Linq;

namespace saongroup.jobdashboard.api.dto.MapperObjects
{
    public class RoleDto
    {
        public int Id { get; set; }

        public string RoleName { get; set; }

        public bool Status { get; set; }

        public static RoleDto FromModel(Role model)
        {
            return new RoleDto()
            {
                Id = model.Id,
                RoleName = model.RoleName,
                Status = model.Status,
            };
        }

        public Role ToModel()
        {
            return new Role()
            {
                Id = Id,
                RoleName = RoleName,
                Status = Status,
            };
        }
    }
}
