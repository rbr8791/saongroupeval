﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace saongroup.jobdashboard.api.common
{
    public class DatabaseConfiguration: ConfigurationBase
    {
        private string DbConnectionKey = "saongroupJobDashboardApiConnection";
        public string GetDatabaseConnectionString()
        {
            return GetConfiguration().GetConnectionString(DbConnectionKey);
        }
    }
}
