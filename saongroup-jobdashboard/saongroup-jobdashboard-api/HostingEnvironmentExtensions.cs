﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace saongroup_jobdashboard_api
{
    public static class HostingEnvironmentExtensions
    {
        public static bool IsProdOrStaging(this IHostingEnvironment env)
        {
            return env.IsProduction() || env.IsStaging();
        }
    }
}
