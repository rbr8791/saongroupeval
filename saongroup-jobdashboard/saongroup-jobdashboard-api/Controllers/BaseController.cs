﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using saongroup.jobdashboard.api.dto.ViewModels;
using saongroup_jobdashboard_api.Helpers;


namespace saongroup_jobdashboard_api.Controllers
{
    public class BaseController : Controller
    {
        protected string IncorrectUseOfApi()
        {
            return CommonHelpers.IncorrectUsageOfApi();
        }

        protected JsonResult ErrorResponse(string message = "Not Found")
        {
            return Json(new
            {
                Success = false,
                Result = message
            });
        }

        protected JsonResult MessageResult(string message, bool success = true)
        {
            return Json(new
            {
                Success = success,
                Result = message
            });
        }

        protected JsonResult SingleResult(BaseViewModel singleResult)
        {
            return Json(new
            {
                Success = true,
                Result = singleResult
            });
        }

        protected JsonResult MultipleResults(IEnumerable<BaseViewModel> multipleResults)
        {
            return Json(new
            {
                Success = true,
                Result = multipleResults
            });
        }
    }
}
