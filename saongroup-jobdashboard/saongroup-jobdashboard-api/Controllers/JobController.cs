﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using saongroup.jobdashboard.api.dto.MapperObjects;
using saongroup.jobdashboard.api.dal;
using saongroup.jobdashboard.api.entities;
using saongroup.jobdashboard.api.dal.Interfaces;
using saongroup.jobdashboard.api.dto.Helpers;
using AutoMapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Configuration;
using Microsoft.AspNetCore.Authorization;
using saongroup_jobdashboard_api.Helpers;
using saongroup_jobdashboard_api.Controllers;

namespace saongroup_jobdashboard_api.Controllers
{
    [Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [Produces("application/json")]
    [ApiController]
    public class JobController : BaseController
    {
        private readonly IJobService _jobService;
        private IMapper _mapper;


        public JobController(IJobService jobService,
            IMapper mapper)
        {
            _jobService = jobService;
            _mapper = mapper;
        }


        [HttpGet("Get/{id}")]
        public JsonResult GetByOrdinal(int id)
        {
            var job = _jobService.FindByOrdinal(id);
            if (job == null)
            {
                return ErrorResponse("Not found");
            }

            return SingleResult(JobViewModelHelper.ConvertToViewModel(job));
        }


        [HttpGet]
        public IActionResult GetAll()
        {
            var jobs = _jobService.GetAll();
            var jobsDTO = _mapper.Map<IList<JobDto>>(jobs);
            return Ok(jobsDTO);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var job = _jobService.GetById(id);
            var jobDTO = _mapper.Map<JobDto>(job);
            return Ok(jobDTO);
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _jobService.Delete(id);
            return Ok();
        }


        [HttpPost("Create")]
        public IActionResult Create([FromBody]JobDto jobDto)
        {
            var job = _mapper.Map<Job>(jobDto);
            try
            {
                var ex = jobDto.ExpiresAt.ToString("yyyy-MM-dd HH:mm:ss tt");
                _jobService.Create(job, jobDto.JobTitle, jobDto.Description, ex);
                return Ok(new
                {
                    message = "Ok"
                });
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        } // Create

        
        [HttpPost("Update")]
        public IActionResult Update([FromBody]JobDto jobDto)
        {
            var job = _mapper.Map<Job>(jobDto);
            try
            {
                var ex = jobDto.ExpiresAt.ToString("yyyy-MM-dd HH:mm:ss tt");
                _jobService.Update(job, jobDto.JobTitle, jobDto.Description, ex);
                return Ok(new
                {
                    message = "Ok"
                });
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        } // Create



    } // End Class
}
