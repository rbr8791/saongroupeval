﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using saongroup.jobdashboard.api.dto.MapperObjects;
using saongroup.jobdashboard.api.dal;
using saongroup.jobdashboard.api.entities;
using saongroup.jobdashboard.api.dal.Interfaces;
using saongroup.jobdashboard.api.dto.Helpers;
using AutoMapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Configuration;
using Microsoft.AspNetCore.Authorization;
using saongroup_jobdashboard_api.Helpers;
using saongroup_jobdashboard_api.Controllers;

namespace saongroup_jobdashboard_api.Controllers
{
    [Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [Produces("application/json")]
    [ApiController]
    public class RoleController : BaseController
    {
        private readonly IRoleService _roleService;
        private IMapper _mapper;


        public RoleController(IRoleService roleService,
            IMapper mapper)
        {
            _roleService = roleService;
            _mapper = mapper;
        }


        [HttpGet("Get/{id}")]
        public JsonResult GetByOrdinal(int id)
        {
            var role = _roleService.FindByOrdinal(id);
            if (role == null)
            {
                return ErrorResponse("Not found");
            }

            return SingleResult(RoleViewModelHelper.ConvertToViewModel(role));
        }


        [HttpGet]
        public IActionResult GetAll()
        {
            var roles = _roleService.GetAll();
            var rolesDTO = _mapper.Map<IList<RoleDto>>(roles);
            return Ok(rolesDTO);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var role = _roleService.GetById(id);
            var roleDTO = _mapper.Map<RoleDto>(role);
            return Ok(roleDTO);
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _roleService.Delete(id);
            return Ok();
        }
                     

    } // End Class
}
