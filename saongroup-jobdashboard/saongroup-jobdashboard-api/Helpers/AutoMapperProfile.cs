﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using saongroup.jobdashboard.api.dto.MapperObjects;
using saongroup.jobdashboard.api.entities;

namespace saongroup_jobdashboard_api.Helpers
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            
            CreateMap<Role, RoleDto>();
            CreateMap<RoleDto, Role>();
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
            CreateMap<Job, JobDto>();
            CreateMap<JobDto, Job>();

        }
    }
}
